import React, { Component } from "react";

export default class ProfileDeleteSetting extends Component {
  render() {
    return (
      //delete the profile starts here
      <form>
        <h1>Delete your profile</h1>
        <h2>
          Are you sure you want to delete your profile ?<br /> All your matchs
          will be lost...
        </h2>
        <div className="input-group">
          <input type="password" />
          <label>password</label>
        </div>
        <div className="btn1">Confirm</div>
      </form>
      // delete the profile ends here
    );
  }
}
