import React, { Component } from "react";
const token = localStorage.getItem("token");

export default class AddressAccountSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: [],
    };
  }

  componentDidMount() {
    fetch("http://localhost:9000/api/address", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((addressList) => {
        this.setState({ address: addressList });
        console.log({ addressList });
      });
  }
  render() {
    return (
      <>
        <h1>Your saved Address: </h1>
        {this.state.address.map((address_item) => (
          <div>
            <div className="input-group">
              <h4>
                Full address: <br />{" "}
                <span style={{ backgroundColor: "#FFFF00" }}>
                  {address_item.address}
                </span>
              </h4>
            </div>
            <div className="input-group">
              <h4>
                City:{" "}
                <span style={{ backgroundColor: "#FFFF00" }}>
                  {address_item.city}
                </span>
              </h4>
            </div>
            <div className="input-group">
              <h4>
                Pincode:{" "}
                <span style={{ backgroundColor: "#FFFF00" }}>
                  {address_item.postCode}
                </span>
              </h4>
            </div>
          </div>
        ))}
        {/* <!-- Button trigger modal --> */}
        <button
          type="button"
          class="btn btn-primary"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Update address or Add Address
        </button>

        {/* <!-- Modal --> */}
        <div
          class="modal fade"
          id="exampleModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Modal title
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                {/* // address change starts here */}
                <form>
                  <h1>Your saved Address</h1>

                  <div className="input-group">
                    <label>Full address: </label>
                    <input name="full_address" type="text" />
                  </div>
                  <div className="input-group">
                    <label>City</label>
                    <input name="City" type="text" />
                  </div>
                  <div className="input-group">
                    <label>Pincode</label>
                    <input name="Pincode" type="text" />
                  </div>

                  <div className="btn1">Save Change</div>
                </form>
                {/* // address change ends here */}
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" class="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
