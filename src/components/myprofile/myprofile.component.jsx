import React from "react";
import "./myprofile.styles.scss";
import GeneralAccountSetting from "./generalaccountsettings";
import PasswordAccountSetting from "./passwordaccountsettings";
import AddressAccountSetting from "./addressaccountsettings";
import ProfileDeleteSetting from "./profiledeletesettings";

const token = localStorage.getItem("token");
class MyProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      openTab: 0,
    };
    this.switchTab = this.switchTab.bind(this);
  }

  componentDidMount() {
    fetch("http://localhost:9000/api/account", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((usersList) => {
        this.setState({ users: usersList });
        console.log({ usersList });
      });
  }

  switchTab(index) {
    this.setState({ openTab: index });
  }

  render() {
    const { openTab } = this.state;

    return (
      <div className="settings-container">
        <div className="left-column">
          <ul>
            <li>
              <i className="fas fa-user"></i>Username:{" "}
              <u> {this.state.users.login}</u>
            </li>
            <li onClick={this.switchTab.bind(this, 0)}>
              <i className="fas fa-user"></i>General
            </li>
            <li onClick={this.switchTab.bind(this, 1)}>
              <i className="fas fa-shield-alt"></i>Password
            </li>
            <li onClick={this.switchTab.bind(this, 2)}>
              <i className="fas fa-ban"></i>Address
            </li>
            <li onClick={this.switchTab.bind(this, 3)}>
              <i className="fas fa-minus-circle"></i>Delete profile
            </li>
          </ul>
        </div>
        <div className="settings-tab">
          {openTab === 0 && <GeneralAccountSetting />}
          {openTab === 1 && <PasswordAccountSetting />}
          {openTab === 2 && <AddressAccountSetting />}
          {openTab === 3 && <ProfileDeleteSetting />}
        </div>
      </div>
    );
  }
}
export default MyProfile;
