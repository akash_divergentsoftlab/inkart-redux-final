import React, { Component } from "react";

const token = localStorage.getItem("token");

export default class GeneralAccountSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    fetch("http://localhost:9000/api/account", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        Authorization: "Bearer " + token,
      },
    })
      .then((response) => response.json())
      .then((usersList) => {
        this.setState({ users: usersList });
        console.log({ usersList });
      });
  }
  render() {
    return (
      <>
        <h1>General account Settings</h1>
        <div className="input-group">
          <h4>
            First Name :{" "}
            <span style={{ backgroundColor: "#FFFF00" }}>
              {this.state.users.firstName}
            </span>
          </h4>
        </div>
        <div className="input-group">
          <h4>
            Last Name:{" "}
            <span style={{ backgroundColor: "#FFFF00" }}>
              {this.state.users.lastName}
            </span>
          </h4>
        </div>
        <div className="input-group">
          <h4>
            Email:{" "}
            <span style={{ backgroundColor: "#FFFF00" }}>
              {this.state.users.email}
            </span>
          </h4>
        </div>
        <div className="input-group">
          <h4>
            Mobile Number: <span style={{ backgroundColor: "#FFFF00" }}></span>
          </h4>
        </div>
        {/* // <!-- Button trigger modal --> */}
        <button
          type="button"
          class="btn btn-info"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Update General Settings
        </button>
        {/* // <!-- Modal --> */}
        <div
          class="modal fade"
          id="exampleModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                  Update General Settings
                </h5>
                <button
                  type="button"
                  class="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                {/* // genereal account settings starts here */}{" "}
                <form>
                  <h1>General account Settings</h1>{" "}
                  <div className="input-group">
                    <label Name>First Name</label>
                    <input name="first_name" type="text" />{" "}
                  </div>{" "}
                  <div className="input-group">
                    <label>Last Name</label>
                    <input name="last_name" type="text" />{" "}
                  </div>{" "}
                  <div className="input-group">
                    <label>Email</label>
                    <input name="last_name" type="email" />{" "}
                  </div>{" "}
                  <div className="input-group">
                    <label>Mobile Number</label>
                    <input name="mobile_number" type="text" />{" "}
                  </div>
                </form>
                {/* // genereal account settings ends here */}
              </div>
              <div class="modal-footer">
                <button
                  type="button"
                  class="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
                <button type="button" class="btn btn-primary">
                  Save changes
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
