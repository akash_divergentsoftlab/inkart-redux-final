import React, { useState } from "react";

const token = localStorage.getItem("token");

export default function PasswordAccountSetting() {
  const [oldPassword, setOldPassWord] = useState("");
  const [newPassword, setnewPassWord] = useState("");

  // eslint-disable-next-line
  async function changePassword() {
    let item = { oldPassword, newPassword };

    let result = await fetch(
      "http://localhost:9000/api/account/change-password",
      {
        method: "POST",
        body: JSON.stringify(item),
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
          Authorization: "Bearer " + token,
        },
      }
    );
    result = await result.json();
    console.warn("result", result);
  }

  // headers do baar aa gaya hai isme

  return (
    <>
      {/* <!-- Button trigger modal --> */}
      <button
        type="button"
        class="btn btn-info"
        data-toggle="modal"
        data-target="#exampleModal"
      >
        Change Your Password
      </button>

      {/* <!-- Modal --> */}
      <div
        class="modal fade"
        id="exampleModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">
                Change Your Password
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              {" "}
              {/* // change password starts here */}
              <h1>Change password</h1>
              <div className="input-group">
                <label>old password</label>
                <input
                  type="password"
                  value={oldPassword}
                  onChange={(e) => setOldPassWord(e.target.value)}
                />
              </div>
              <div className="input-group">
                <label>new password</label>
                <input
                  type="password"
                  value={newPassword}
                  onChange={(e) => setnewPassWord(e.target.value)}
                />
              </div>
            </div>
            <div class="modal-footer">
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
              <button type="button" class="btn btn-primary">
                Save changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
    //main settings ends here
  );
}
