import React, { Component } from "react";

export default class LogoutPage extends Component {
  constructor(props) {
    super(props);
    localStorage.removeItem("token");
    this.props.history.push("/login");
  }

  render() {
    return (
      <>
        <h2>You have been logged out!</h2>
      </>
    );
  }
}
