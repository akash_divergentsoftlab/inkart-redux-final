import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./productDetail.styles.scss";

import { Link } from "react-router-dom";
import { URL_ADDRESS } from "../../../Default";

function ProductDetail() {
  const [productDetail, setproductDetail] = useState([]);
  const { productId } = useParams();

  const getproductDetail = () =>
    fetch(`${URL_ADDRESS}/product/find/${productId}`).then((res) => res.json());

  useEffect(() => {
    getproductDetail().then((productDetail) => setproductDetail(productDetail));// eslint-disable-next-line
  }, []);

  return (
    <div className="container">
      <div className="detail-container rounded my-5 row">
        <div className="col-md-12 ">
          <button className="btn bg-light" style={{ marginRight: "971px" }}>
            <Link to="/shop/">Go Back</Link>
          </button>
          <div className="row">
            <div className="rounded-left col-md-6 product bg-white">
              <img
                className="mt-5"
                src={productDetail.imageUrl}
                style={{ width: "443px", height: "271px" }}
                alt=""
              />
              <div className="color text-center">
                <h5 className="mb-4">Colors</h5>
                <span>{productDetail.colour}</span>
              </div>
            </div>
            <div className="rounded-right col-md-6 detail bg-dange pb-5">
              <div className="py-5 px-4">
                <h3 className="py-5 ">{productDetail.productName}</h3>
                <b>Description:</b>
                {productDetail.description}
                <br />
                <b>Brand: </b>
                {productDetail.brandName}
              </div>
              <div className="text-center price">
                <span>₹{productDetail.price}</span>
              </div>
              <div className="text-center mt-5">
                <button className="btn bg-warning ml-7">Add to cart</button>
              </div>
              <i className="fa fa-share-square-o" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProductDetail;
