// import {
//   Accordion,
//   AccordionDetails,
//   AccordionSummary,
//   Card,
//   CardActions,
//   CardContent,
//   CardMedia,
//   Divider,
//   Grid,
//   IconButton,
//   Typography,
// } from "@material-ui/core";
// import { AddShoppingCart } from "@material-ui/icons";
// import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useState } from "react";
import { useDispatch } from "react-redux";
import allActions from "../../../store/actions";
// import Modal from "../../Modal/Modal";
import { Link } from "react-router-dom";
import "./productContainer.styles.scss";

const Product = ({ productId, productName, price, imageUrl }) => {
  const [showModal, setShowModal] = useState(false);
  // eslint-disable-next-line
  const [expanded, setExpanded] = useState(null);
  const dispatch = useDispatch();
  // eslint-disable-next-line
  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  return (
    <>
      <ul className="auto-grid">
        <li
          class="product"
          style={{
            padding: "0rem -1rem",
            textAlign: "center",
            fontSize: "14px",
            background: "#ffffff",
            color: "#ffffff",
          }}
        >
          <Link class="img-wrapper" to={`/product-detail/${productId}`}>
            <img
              src={imageUrl}
              alt={productName}
              style={{ height: "206px", width: "247px" }}
              onClick={() => setShowModal(!showModal)}
            />
          </Link>
          <div class="info">
            <div class="title">
              <Link to={`/product-detail/${productId}`}>
                {productName.slice(0, 25)}
              </Link>
            </div>
            <div class="price">₹{price}</div>
          </div>
          <div class="actions-wrapper">
            <a href="/#" class="add-btn wishlist">
              {" "}
              Wishlist{" "}
            </a>
            <button
              href="/#"
              onClick={() =>
                dispatch(
                  allActions.cartActions.addToCart(
                    productId,
                    productName,
                    price,
                    imageUrl
                  )
                )
              }
              class="add-btn cart"
            >
              {" "}
              Cart{" "}
            </button>
          </div>
        </li>
      </ul>
    </>

    // <Card className={classes.root}>
    //   <CardMedia
    //     className={classes.media}
    //     classes={{
    //       root: classes.media,
    //     }}
    //     image={imageUrl}
    //     title={productName}
    //     onClick={() => setShowModal(!showModal)}
    //   />
    //   <CardContent className={classes.cardContent}>
    //     <Grid container align="center" spacing={1}>
    //       <Grid item xs={12}>
    //         <Accordion
    //           expanded={expanded === 'panel1'}
    //           onChange={handleChange('panel1')}
    //           variant="outlined"
    //         >
    //           <AccordionSummary
    //             aria-controls="panel1d-content"
    //             id="panel1d-header"
    //             expandIcon={<ExpandMoreIcon />}
    //             aria-label="Toggle product description"
    //             title="Click to toggle description"
    //           >
    //             <Typography variant="h6" component="h2">
    //               {productName}
    //             </Typography>
    //           </AccordionSummary>
    //           <Divider />
    //           <AccordionDetails className={classes.accordion}>
    //             <Typography
    //               gutterBottom
    //               variant="body2"
    //               component="p"
    //               color="textPrimary"
    //               className={classes.description}
    //               align="left"
    //             >
    //               {description}
    //             </Typography>
    //           </AccordionDetails>
    //         </Accordion>
    //       </Grid>
    //       <Grid item xs={6}>
    //         <Typography
    //           gutterBottom
    //           variant="overline"
    //           component="p"
    //           // align="left"
    //           color="textPrimary"
    //         >
    //           {subCategoryName}
    //         </Typography>
    //       </Grid>
    //       <Grid item xs={6}>
    //         <Typography
    //           gutterBottom
    //           variant="h6"
    //           component="h3"
    //           // align="right"
    //           className={classes.price}
    //           title={price.toFixed(2)}
    //         >
    //           €{price.toFixed(2)}
    //         </Typography>
    //       </Grid>
    //     </Grid>
    //     <Divider />
    //   </CardContent>
    //   <CardActions disableSpacing className={classes.cardActions}>
    //     <IconButton
    //       aria-label="Add to Cart"
    //       onClick={() =>
    //         dispatch(
    //           allActions.cartActions.addToCart(
    //             productId,
    //             productName,
    //             price,
    //             description,
    //             subCategoryName,
    //             imageUrl,
    //             quantity
    //           )
    //         )
    //       }
    //     >
    //       <Typography variant="button" display="block" color="textPrimary">
    //         Add to cart
    //       </Typography>
    //       <AddShoppingCart />
    //     </IconButton>
    //   </CardActions>
    //   {showModal && (
    //     <Modal showModal={showModal} setShowModal={setShowModal} img={imageUrl} />
    //   )}
    // </Card>
  );
};

export default Product;
