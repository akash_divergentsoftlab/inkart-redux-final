import { Badge, Button, IconButton } from "@material-ui/core";
import { ShoppingCart } from "@material-ui/icons";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useSelector } from "react-redux";
import { Link, NavLink, useHistory, useLocation } from "react-router-dom";
import { CustomTooltip, Search } from "../../components";
import CategoryNavbar from "./CategoryNavbar";

const selectAmount = (state) => state.cart.amount;

const Navbar = () => {
  const amount = useSelector(selectAmount);
  const { pathname } = useLocation();
  const history = useHistory();

  const handleClick = () => {
    history.goBack();
  };

  // timer
  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-light bg-dark navbar-
            fixed-top"
      >
        <Link
          className="navbar-brand"
          to="/"
          style={{
            color: "white",
            marginLeft: "160px",
            marginTop: "-14px",
            marginRight: "10px",
          }}
        >
          <b>
            <i>Inkart</i>
          </b>
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          {pathname === "/" ? (
            <Search />
          ) : (
            <Button
              onClick={handleClick}
              type="button"
              variant="contained"
              color="primary"
              startIcon={<ArrowBackIcon />}
              aria-label="Go back"
              title="Go back"
              size="medium"
            >
              Go Back
            </Button>
          )}
          <Link className="_3ko_Ud" to="/shop/" style={{ marginLeft: "35px" }}>
            <span
              style={{
                color: "whitesmoke",
                fontSize: "18",
                marginLeft: "149px",
              }}
            >
              <b>Shop</b>
            </span>
          </Link>
          {localStorage.getItem("token") ? (
            <>
              <li className="nav-item dropdown" style={{ listStyle: "none" }}>
                <a
                  className="nav-link dropdown-toggle"
                  id="navbarDropdown"
                  role="button"
                  href="/#"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  style={{
                    color: "white",
                    marginTop: "1px",
                    marginLeft: "25px",
                  }}
                >
                  <b> More</b>
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <Link
                    aria-hidden="true"
                    to="/myprofile"
                    style={{ marginLeft: "1px" }}
                  >
                    My Account info
                  </Link>
                  <div className="dropdown-divider"></div>
                  <a aria-hidden="true" href="/#" style={{ marginLeft: "1px" }}>
                    Account Settings
                  </a>
                  <div className="dropdown-divider"></div>
                  <Link aria-hidden="true" to="/logout">
                    Logout
                  </Link>
                </div>
              </li>
            </>
          ) : (
            <>
              <p
                style={{
                  color: "white",
                  marginBottom: "1px",
                  marginLeft: "190px",
                }}
              >
                <Link to="/login">
                  <b>Login & Signup</b>
                </Link>
              </p>
            </>
          )}

          <CustomTooltip title="Show cart items">
            <IconButton
              component={NavLink}
              to="/cart"
              aria-label="Show cart items"
              color="inherit"
              activeStyle={{
                background: "rgba(227, 212, 152, 0.6)",
              }}
            >
              <Badge badgeContent={amount} color="secondary" showZero>
                <ShoppingCart />
              </Badge>
            </IconButton>
          </CustomTooltip>
        </div>
      </nav>
      <br />
      <br />
      <br />
      <CategoryNavbar />
    </>
  );
};

export default Navbar;
