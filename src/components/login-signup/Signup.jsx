import React, { useState } from "react";
import "./Signup.styles.scss";
import { Link } from "react-router-dom";
import { URL_ADDRESS } from "../../Default";

export default function Signup() {
  const [email, setEmail] = useState("");
  const [login, setLogin] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");

  async function signUp() {
    let item = { email, login, firstName, lastName, password };
    console.log(item);

    let result = await fetch(`${URL_ADDRESS}/register`, {
      method: "POST",
      body: JSON.stringify(item),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    });
    result = await result.json();
    console.warn("result", result);
  }

  return (
    <div>
      <div class="wrapper fadeInDown">
        <div id="formContent">
          <form>
            <input
              type="text"
              id="login"
              class="fadeIn second"
              name="login"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="email"
            />
            <input
              type="text"
              id="login"
              class="fadeIn second"
              name="login"
              value={login}
              onChange={(e) => setLogin(e.target.value)}
              placeholder="username"
            />
            <input
              type="text"
              id="login"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              class="fadeIn second"
              name="login"
              placeholder="first name"
            />
            <input
              type="text"
              id="login"
              class="fadeIn second"
              name="login"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              placeholder="last name"
            />
            <input
              type="text"
              id="password"
              class="fadeIn third"
              name="login"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              placeholder="password"
            />

            <button class=" btn bg-primary fadeIn fourth" Click={signUp}>
              Signup
            </button>
          </form>

          <div id="formFooter">
            <Link class="underlineHover" to="/login">
              Already have a account? Sign here
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
