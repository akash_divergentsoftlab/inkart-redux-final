import React, { Component } from "react";
import "./Login.styles.scss";
import { Link } from "react-router-dom";
import { URL_ADDRESS } from "../../Default";

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: null,
      password: null,
      login: false,
      store: null,
    };
  }

  login() {
    fetch(`${URL_ADDRESS}/authenticate`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(this.state),
    }).then((response) => {
      response.json().then((result) => {
        console.warn("result", result);
        localStorage.setItem("token", result.id_token);

        this.setState({ login: true });
      });
    });
  }

  render() {
    return (
      <div>
        {!localStorage.getItem("token") ? (
          <>
            <div class="wrapper fadeInDown">
              <div id="formContent">
                {/* <!-- Tabs Titles --> */}

                {/* <!-- Login Form --> */}

                <input
                  type="text"
                  id="login"
                  class="fadeIn second"
                  name="login"
                  placeholder="username"
                  onChange={(event) => {
                    this.setState({
                      username: event.target.value,
                    });
                  }}
                />
                <input
                  type="text"
                  id="password"
                  class="fadeIn third"
                  name="login"
                  placeholder="password"
                  onChange={(event) => {
                    this.setState({
                      password: event.target.value,
                    });
                  }}
                />
                <button
                  onClick={() => {
                    this.login();
                  }}
                  class=" btn bg-primary fadeIn fourth"
                >
                  Sign in
                </button>

                {/* <!-- Remind Passowrd --> */}
                <div id="formFooter">
                  <Link class="underlineHover" to="/signup">
                    Don't have a account? Signup here.
                  </Link>
                </div>
              </div>
            </div>
          </>
        ) : (
          <>
            <h1>You have been already logged in !!</h1>
          </>
        )}
      </div>
    );
  }
}
