import React from "react";
import "./cartitem.scss";
import { useDispatch } from "react-redux";
import {
  Button,
  // Card,
  CardActions,
  // CardContent,
  // CardMedia,
  // Divider,
  Typography,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
// import DeleteIcon from '@material-ui/icons/Delete';
import RemoveIcon from "@material-ui/icons/Remove";
import { memo, useState } from "react";
import { CustomTooltip, Modal } from "../../..";
import allActions from "../../../../store/actions";
import useStyles from "./styles";

const CartItem = memo(({ id, image, title, price, qty }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const totalPrice = (qty * price).toFixed(2);

  return (
    <div>
        <div class="container">
          <table id="cart" class="table table-hover table-condensed">
            <thead>
              <tr>
                <th style={{ width: "88%" }}>Product</th>
                <th style={{ width: "10%" }}>Price</th>
                <th style={{ width: "8%" }}>Quantity</th>
                <th style={{ width: "22%" }} class="text-center">
                  Subtotal
                </th>
                <th style={{ width: "10%" }}></th>
              </tr>
            </thead>
            <tbody>
      <tr>
        <td data-th="Product">
          <div class="row">
            <div class="col-sm-10">
              <h4 class="nomargin">{title.slice(0, 25)}</h4>
            </div>
          </div>
        </td>
        <td data-th="Price">
          <b>₹{price}</b>
        </td>
        <td data-th="Quantity">
          <CardActions className={classes.cardActions}>
            <Typography variant="h6"></Typography>
            <div className={classes.rightBtns}>
              <CustomTooltip title="Remove one item from cart">
                <Button
                  type="button"
                  onClick={() => dispatch(allActions.cartActions.decrease(id))}
                  variant="outlined"
                  style={{width:"2px"}}
                  disabled={qty <= 1}
                  className={classes.btnDel}
                  classes={{ root: classes.btnRoot }}
                  aria-label="remove one item from cart"
                  component="span"
                >
                  <RemoveIcon />
                </Button>
              </CustomTooltip>
              <Typography variant="h6" component="p">
                <strong>{qty}</strong>
              </Typography>
              <CustomTooltip title="Add one additional item to cart">
                <Button
                  style={{width:"2px"}}
                  type="button"
                  variant="outlined"
                  onClick={() => dispatch(allActions.cartActions.increase(id))}
                  className={classes.btnAdd}
                  classes={{ root: classes.btnRoot }}
                  aria-label="add one more item to cart"
                >
                  <AddIcon />
                </Button>
              </CustomTooltip>
            </div>
          </CardActions>
        </td>
        <td data-th="Subtotal" class="text-center">
          ₹{totalPrice}
        </td>
        <td class="actions" data-th="">
          <button
            onClick={() => dispatch(allActions.cartActions.removeItem(id))}
            class="btn btn-danger btn-sm"
          >
            <i class="fa fa-trash-o"></i>
          </button>
        </td>
      </tr>
      </tbody>
          </table>
        </div>
    </div>
  );
});

export default CartItem;
