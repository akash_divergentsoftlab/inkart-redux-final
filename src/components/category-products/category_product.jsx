import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { URL_ADDRESS } from "../../Default";

function CategoryProduct() {
  const [categoryProduct, setcategoryProduct] = useState([]);
  const { subcategoryName } = useParams();

  const getcategoryProduct = () =>
    fetch(`${URL_ADDRESS}/product/${subcategoryName}`).then((res) =>
      res.json()
    );

  useEffect(() => {
    getcategoryProduct().then((categoryProduct) =>
      setcategoryProduct(categoryProduct)
    );// eslint-disable-next-line
  }, []);
  return (
    <>
      {categoryProduct.map((product) => (
        <li
          class="product"
          key={product.productId}
          style={{
            padding: "3rem 0rem",
            textAlign: "center",
            fontSize: "1.2rem",
            background: "#ffffff",
            color: "#ffffff",
          }}
        >
          <a class="img-wrapper" href="/#">
            <img
              src={product.imageUrl}
              alt="Blue running shoe"
              style={{ height: "206px", width: "247px" }}
            />
          </a>
          <div class="info">
            <div class="title">{product.productName.slice(0, 20)}</div>
            <div class="price">$34.99</div>
          </div>
          <div class="actions-wrapper">
            <a href="/#" class="add-btn wishlist">
              {" "}
              Wishlist{" "}
            </a>
            <a href="/#" class="add-btn cart">
              {" "}
              Cart{" "}
            </a>
          </div>
        </li>
      ))}
    </>
  );
}

export default CategoryProduct;
