import React from "react";
import Shop from "../components/shoppage/Shop";

export default function ShopPage() {
  return (
    <div>
      <Shop />
    </div>
  );
}
