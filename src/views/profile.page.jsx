import React from "react";
import MyProfile from "../components/myprofile/myprofile.component";

function MyProfilePage() {
  return (
    <div>
      <MyProfile />
    </div>
  );
}

export default MyProfilePage;
