import React from "react";
import ProductDetail from "../components/Products/productDetail/productDetail";

function ProductDetailPage() {
  return (
    <div className="product-detail-page">
      <ProductDetail />
    </div>
  );
}

export default ProductDetailPage;
