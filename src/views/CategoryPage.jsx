import React from "react";
import CategoryProductContainer from "../components/category-products/product-container";

function CategoryPage() {
  return (
    <div>
      <CategoryProductContainer />
    </div>
  );
}

export default CategoryPage;
