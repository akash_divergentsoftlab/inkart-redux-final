import CssBaseline from "@material-ui/core/CssBaseline";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Alert, Navbar } from "./components";
import allActions from "./store/actions";
import { Checkout, Error, Home, ShoppingCart } from "./views";
import LoginPage from "./views/LoginPage";
import SignUpPage from "./views/SignUpPage";
import ProductDetailPage from "./views/ProductDetailPage";
import MyProfilePage from "./views/profile.page";
import CategoryPage from "./views/CategoryPage";
import ShopPage from "./views/ShopPage";
import LogoutPage from "./components/logout/LogOut";

const selectAlert = (state) => state.cart.alert;
const selectCart = (state) => state.cart.cart;

const App = () => {
  const cart = useSelector(selectCart);
  const alert = useSelector(selectAlert);
  const dispatch = useDispatch();

  useEffect(
    () => dispatch(allActions.productsActions.fetchProducts()),
    [dispatch]
  );

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
    dispatch(allActions.cartActions.getTotals());
  }, [cart, dispatch]);

  return (
    <Router>
      <CssBaseline />
      <Navbar />
      {alert.show && <Alert />}
      <Switch>
        <Route exact path="/" component={Home} />
        <Route
          exact
          path="/product-detail/:productId"
          component={ProductDetailPage}
        />
        <Route exact path="/myprofile" component={MyProfilePage} />
        <Route exact path="/login" component={LoginPage} />
        <Route exact path="/signup" component={SignUpPage} />
        <Route exact path="/cart" component={ShoppingCart} />
        <Route exact path="/shop" component={ShopPage} />
        <Route exact path="/logout" component={LogoutPage} />
        <Route
          exact
          path="/category/:subcategoryName"
          component={CategoryPage}
        />
        <Route exact path="/checkout" component={Checkout} />
        <Route path="*" component={Error} />
      </Switch>
    </Router>
  );
};

export default App;
